# Trossen X-Series Workspace
Files and scripts to bootstrap a workspace for the Trossen Robotics X-Serieis Robot Arms.

---

```bash
mkdir ~/trossen
cd ~/trossen
git clone git+ssh://git@git.sr.ht/~mynameiscosmo/trossen_x-series_workspace

mkdir -p ~/trossen/catkin_ws/src
cp ~/trossen/trossen_x-series_workspace/files/rosinstall ~/trossen/catkin_ws/src/.rosinstall

cd ~/trossen/catkin_ws/src
catkin_init_workspace
wstool update
```
